package com.epam.jamp;

import com.epam.jamp.exception.RollbackCausingException;
import com.epam.jamp.model.User;
import com.epam.jamp.persistance.AccountHibernateRepo;
import com.epam.jamp.persistance.UserHibernateRepo;
import com.epam.jamp.service.MoneyTransferService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.epam.jamp")
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(App.class);
        // transaction showcase
        MoneyTransferService mts = ctx.getBean(MoneyTransferService.class);
        UserHibernateRepo userRepo = ctx.getBean(UserHibernateRepo.class);
        User firstUser = userRepo.findOne(1);
        User secontUser = userRepo.findOne(2);
        mts.transfer(firstUser.getAccounts().get(1).getId(), secontUser.getAccounts().get(0).getId(), 1.0);
        try {
            mts.transferWithInnerException(firstUser.getAccounts().get(1).getId(), secontUser.getAccounts().get(0).getId(), 1.0);
        } catch (RollbackCausingException e) {
            System.out.println("It's awesome!");
        }
    }
}
