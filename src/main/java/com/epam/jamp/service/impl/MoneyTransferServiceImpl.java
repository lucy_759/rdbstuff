package com.epam.jamp.service.impl;

import com.epam.jamp.exception.HoboException;
import com.epam.jamp.model.Account;
import com.epam.jamp.persistance.AccountRepo;
import com.epam.jamp.service.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

//@Service
public class MoneyTransferServiceImpl implements MoneyTransferService {

    @Autowired
    private AccountRepo accountRepo;

    @Transactional
    public void transfer(Long senderAccountId, Long recieverAcoountId, double amount) {
        Account sender = accountRepo.getById(senderAccountId);
        Account reciever = accountRepo.getById(recieverAcoountId);
        if (sender.getBalance() >= amount) {
            accountRepo.updateBalance(senderAccountId, sender.getBalance() - amount);
            accountRepo.updateBalance(recieverAcoountId, reciever.getBalance() + amount);
        } else {
            throw new HoboException("Not ennough money on card", sender.getCardNumber());
        }
    }

    @Transactional
    public void transferWithInnerException(Long senderAccountId, Long recieverAcoountId, double amount) {
        Account sender = accountRepo.getById(senderAccountId);
        Account reciever = accountRepo.getById(recieverAcoountId);
        if (sender.getBalance() >= amount) {
            accountRepo.updateBalance(senderAccountId, sender.getBalance() - amount);
            accountRepo.updateBalanceWithException(recieverAcoountId, reciever.getBalance() + amount);
        } else {
            throw new HoboException("Not ennough money on card", sender.getCardNumber());
        }
    }


}
