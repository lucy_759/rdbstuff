package com.epam.jamp.service.impl;

import com.epam.jamp.exception.HoboException;
import com.epam.jamp.exception.RollbackCausingException;
import com.epam.jamp.model.Account;
import com.epam.jamp.persistance.AccountHibernateRepo;
import com.epam.jamp.service.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component

public class MoneyTransferHibernateServiceImpl implements MoneyTransferService {

    @Autowired
    private AccountHibernateRepo accountRepo;

    @Override
    @Transactional
    public void transfer(Long senderId, Long recieverId, double amount) {
        Account sender = accountRepo.findOne(senderId);
        Account reciver = accountRepo.findOne(recieverId);
        if (sender.getBalance() >= amount) {
            sender.setBalance(sender.getBalance() - amount);
            reciver.setBalance(reciver.getBalance() + amount);
            accountRepo.update(sender);
            accountRepo.update(reciver);
        } else {
            throw new HoboException("Not ennough money on card", sender.getCardNumber());
        }
    }

    @Override
    @Transactional(rollbackFor = RollbackCausingException.class)
    public void transferWithInnerException(Long senderId, Long recieverId, double amount) {
        Account sender = accountRepo.findOne(senderId);
        sender.setBalance(sender.getBalance() - 100);
        accountRepo.update(sender);
        throw new RollbackCausingException("An exception that cause rollback");
    }
}
