package com.epam.jamp.service;

public interface MoneyTransferService {

    public void transfer(Long senderId, Long recieverId, double amount);

    public void transferWithInnerException(Long senderId, Long recieverId, double amount);

}
