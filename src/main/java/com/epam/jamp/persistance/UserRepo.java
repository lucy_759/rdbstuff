package com.epam.jamp.persistance;

import com.epam.jamp.model.Account;
import com.epam.jamp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

//@Repository
public class UserRepo {
    private static final String GET_BY_ID = "SELECT dummy_user.id AS user_id, dummy_user.name AS user_name, " +
            "account.id AS account_id, account.card_number AS card_number, account.card_balance AS card_balance " +
            "FROM dummy_user " +
            "LEFT JOIN account " +
            "ON dummy_user.id = account.user_id " +
            "WHERE user_id = ?";
    private static final String CREATE_USER = "INSERT INTO dummy_user (id, name) VALUES (?, ?)";
    private static final String DELETE_USER = "DELETE DROM dummy_user WHERE dummy_user.id = ?";
    private static final String UPDATE_USER_NAME = "UPDATE dummy_user SET dummy_user.name = ? WHERE dummy_user.id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    public User getById(long id) {
        return jdbcTemplate.query(GET_BY_ID, this::extractData, id);
    }

    public int createUser(User user) {
        return jdbcTemplate.update(CREATE_USER, user.getId(), user.getName());

    }

    public int deleteUser(long id) {
        return jdbcTemplate.update(DELETE_USER, id);
    }

    public int updateUserName(long id, User user) {
        return jdbcTemplate.update(UPDATE_USER_NAME, user.getName(), id);
    }

    public int addUserWithSimpleJdbc(User user){
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("user");
        Map<String, Object> params = new HashMap<>();
        params.put("id", user.getId());
        params.put("name", user.getName());
        return jdbcInsert.execute(params);
    }
    private User extractData(ResultSet rs) throws SQLException {
        User user = new User();
        List<Account> accounts = new ArrayList<>();
        while (rs.next()) {
            if (Objects.isNull(user.getId()) && Objects.isNull(user.getName())) {
                user.setId(rs.getLong("user_id"));
                user.setName(rs.getString("user_name"));
            }
            Account account = new Account();
            account.setId(rs.getLong("account_id"));
            account.setCardNumber(rs.getLong("card_number"));
            account.setBalance(rs.getDouble("card_balance"));
            accounts.add(account);
        }
        user.setAccounts(accounts);
        return user;
    }

}
