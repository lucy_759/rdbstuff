package com.epam.jamp.persistance;

import com.epam.jamp.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//@Repository
public class AccountRepo {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private final String GET_BY_ID = "SELECT * " +
            "FROM account " +
            "WHERE id = ?";
    private final String UPDATE_BALANCE = "UPDATE account " +
            "SET card_balance = ?" +
            "WHERE account.id = ?";
    private final String INVALID_QUERY = "asddsaasddsa";
    private final String INSERT_QUERY = "INSERT INTO account (id, card_number, card_balance, user_id) VALUES (?, ?, ?, ?)";

    public Account getById(Long id) {
        return jdbcTemplate.query(GET_BY_ID, this::extractData, id);
    }

    public int updateBalance(Long senderAccountId, double newBallanceAmount) {
        return transactionTemplate.execute(status -> jdbcTemplate.update(UPDATE_BALANCE, newBallanceAmount, senderAccountId));
    }

    public int updateBalanceWithException(Long senderAccountId, double newBallanceAmount) {
        return transactionTemplate.execute(status -> jdbcTemplate.update(INVALID_QUERY, newBallanceAmount, senderAccountId));
    }

    public long batchInsertAccounts(List<Account> accounts) {
        return Stream.of(transactionTemplate.execute(status -> jdbcTemplate.batchUpdate(INSERT_QUERY, accounts, 5, this::setValues)))
                .flatMapToInt(IntStream::of)
                .count();
    }

    private Account extractData(ResultSet rs) throws SQLException {
        Account account = new Account();
        while (rs.next()) {
            account.setId(rs.getLong("id"));
            account.setCardNumber(rs.getLong("card_number"));
            account.setBalance(rs.getDouble("card_balance"));
        }
        return account;
    }

    public void setValues(PreparedStatement ps, Account account) throws SQLException {
        ps.setLong(1, account.getId());
        ps.setLong(2, account.getCardNumber());
        ps.setDouble(3, account.getBalance());
        ps.setLong(4, 1L);
    }

}
