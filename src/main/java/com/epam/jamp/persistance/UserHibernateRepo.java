package com.epam.jamp.persistance;

import com.epam.jamp.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class UserHibernateRepo {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private EntityManager entityManager;

    public User findOne(long id) {
        return sessionFactory.getCurrentSession().get(User.class, id);
    }

    public List<User> findAll() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<User> q = cb.createQuery(User.class);
        Root<User> userRoot = q.from(User.class);
        CriteriaQuery<User> select = q.select(userRoot);
        TypedQuery<User> query = entityManager.createQuery(q);
        List<User> resultList = query.getResultList();

        return resultList;
    }

    public void create(final User entity) {
        sessionFactory.getCurrentSession().saveOrUpdate(entity);
    }

    public User update(final User entity) {
        return (User) sessionFactory.getCurrentSession().merge(entity);
    }

    public void delete(final User entity) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaDelete<User> delete = builder.createCriteriaDelete(User.class);
        Root<User> root = delete.from(User.class);
        delete.where(builder.equal(root.get("id"), entity.getId()));
        entityManager.createQuery(delete).executeUpdate();
    }

    public void deleteById(final long entityId) {
        User entity = findOne(entityId);
        delete(entity);
    }
}
