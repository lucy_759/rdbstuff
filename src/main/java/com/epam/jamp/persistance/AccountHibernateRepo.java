package com.epam.jamp.persistance;

import com.epam.jamp.model.Account;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountHibernateRepo {

    @Autowired
    private SessionFactory sessionFactory;

    public Account findOne(long id) {
        return sessionFactory.getCurrentSession().get(Account.class, id);
    }

    public List<Account> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from account").list();
    }

    public void create(final Account entity) {
        sessionFactory.getCurrentSession().saveOrUpdate(entity);
    }

    public Account update(final Account entity) {
        return (Account) sessionFactory.getCurrentSession().merge(entity);
    }

    public void delete(final Account entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    public void deleteById(final long entityId) {
        Account entity = findOne(entityId);
        delete(entity);
    }
}
