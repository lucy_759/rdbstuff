package com.epam.jamp.exception;

public class RollbackCausingException extends RuntimeException {
    public RollbackCausingException() {
    }

    public RollbackCausingException(String message) {
        super(message);
    }

    public RollbackCausingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RollbackCausingException(Throwable cause) {
        super(cause);
    }

    public RollbackCausingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
