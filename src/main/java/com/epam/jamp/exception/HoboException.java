package com.epam.jamp.exception;

public class HoboException extends RuntimeException{
    private Long cardNumber;

    public HoboException(String message, Long cardNumber) {
        super(message);
        this.cardNumber = cardNumber;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }
}
